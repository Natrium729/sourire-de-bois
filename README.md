# Sourire de bois #

*Les rideaux se referment. Juste avant qu'ils ne se rejoignent, elle le regarde fugacement dans les yeux et tente de lui sourire. En vain. Ses lèvres ne bougent pas. Elle n'y arrive tout simplement pas. Malgré tous ses efforts, son visage reste impassible. « À la prochaine représentation, je le ferai, » se promet-elle comme à chaque fois.*

## Jouer ##

*Sourire de bois* est une fiction interactive, qui a fini deuxième au [concours de fiction interactive 2015](http://ifiction.free.fr/index.php?id=concours&date=2015). Elle se joue en tapant des verbes à l'infinitif afin de guider l'héroïne.

Si vous voulez essayer, ce n'est pas ici qu'il faut regarder ! Vous pouvez jouer en ligne à la version du concours [à cet endroit](http://ifiction.free.fr/index.php?id=jeu&j=210), et une nouvelle version verra le jour prochainement.

## La Source ##

La source de ce jeu est visualisable dans ce dépôt (dans le fichier « marionnettes.inform/Source/story.ni ») afin de satisfaire votre curiosité. Elle est compilable avec la version 6L38 d'Inform 7.

## Pour m'aider ##

Rien de plus simple : jouez ! Cela me ferait également plaisir si vous me faites part de vos commentaires/impressions/remarques. Vous pouvez me contacter via [mon site](http://ulukos.com/contact/) ou sur [le forum de la communauté francophone dédié aux fictions interactives](http://ifiction.free.fr/taverne/index.php).

Si vous trouvez une erreur dans le jeu, vous pouvez utiliser les mêmes liens que ci-dessus, ou bien l'outil de signalement de bug (menu ci-contre).