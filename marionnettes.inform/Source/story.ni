"Sourire de bois" by Nathanaël Marion (in French)

Volume 1 - Initialisation

Book 1 - Extensions

Include Experimental French Features by Nathanael Marion.

Include (- 
language French

<control-structure-phrase> ::=
	/a/ si ... est début |
	/b/ si ... est |
	/c/ si ... |
	/c/ à moins que |
	/d/ répéter/parcourir ... |
	/e/ tant que ... |
	/f/ sinon |
	/g/ sinon si ... |
	/g/ sinon à moins que |
	/h/ -- sinon |
	/i/ -- ... |

-)  in the Preform grammar.

Include Basic Screen Effects by Emily Short.
Include Glulx Text Effects by Emily Short.

Book 2 - Réponses

Part 1 - Nouveaux adjectifs

In French posé is an adjective.

Part 2 - Réponses personnalisées

La yes or no question internal rule response (A) est "Elle doit se décider. ".

Book 3 - Nouvelles actions

Talking to is an action applying to one visible thing.
Understand "talk to [something]", "speak to [something]", "parler a/au/aux/avec/-- [something]" or "questionner [something]" as talking to.
Understand the command "discuter" as "parler".

Check an actor talking to (this is the can't talk to a non-person rule):
	if the noun is not a person:
		if the actor is the player, dire "[Tu] [ne peux pas] parler [au noun][_]!" (A) instead;
		else stop the action.

Check an actor talking to (this is the talking to yourself rule):
	if the actor is the player and the noun is the player:
		dire "[Tu] [te-se] [conditionally adapt the verb parles in past historic tense] à [toi]-même[if the player is plural-named]s[end if] pendant un moment." (A) instead;
	else if the actor is not the player and the noun is the actor:
		stop the action.

Unsuccessful attempt by someone talking to while the reason the action failed is the talking to yourself rule (this is the other people talking to themselves rule):
	dire "[The actor] [conditionally adapt the verb marmonnes in past historic tense] quelques mots à [lui]-même[if the actor is plural-named]s[end if] pendant un moment." (A).

Report an actor talking to (this is the standard report talking to rule):
	dire "[if the actor is the player][Tu][else][The actor][end if] n['][as] rien à dire." (A).

Rule for clarifying the parser's choice of something (called item) while talking to:
	dire "([au item])[command clarification break]".


Comprendre "arracher [quelque chose]" comme taking.
Understand the command "accrocher" as "nouer".

Book 4 - Nouvelles propriétés

Part 1 - Hors de portée

A thing can be out of reach or nearby. A thing is usually nearby.

The can't touch out of reach things rule is listed before the access through barriers rule in the accessibility rulebook.

Accessibility rule (this is the can't touch out of reach things rule): 
	si l'action requiert un nom touchable et le nom est out of reach:
		dire "[Le nom] [es] trop loin." instead;
	sinon si l'action requiert un second nom touchable and le second nom est out of reach:
		dire "[Le second nom] [es] trop loin." instead.

Volume 2 - Jeu

Book 1 - Introduction

Part 1 - Incipit

When play begins:
	maintenant le point de vue de l'histoire est le third person singular;
	dire "Les rideaux se referment. Juste avant qu'ils ne se rejoignent, elle le regarde fugacement dans les yeux et tente de lui sourire.";
	attendre une touche;
	dire "En vain. Ses lèvres ne bougent pas. Elle n'y arrive tout simplement pas. Malgré tous ses efforts, son visage reste impassible.";
	attendre une touche;
	dire "Les rideaux sont fermés maintenant. Les fils qui la retiennent tombent, suivis de la croix d'attelle.[saut de paragraphe]";
	attendre une touche;
	dire "« À la prochaine représentation, je le ferai, » se promet-elle comme à chaque fois.";
	attendre une touche.

Part 2 - Estrade 1

L' estrade (f) est un endroit. "Le spectacle vient de se terminer. D'expérience, elle sait qu'il lui reste un peu de temps avant qu'on revienne pour la suspendre, jusqu'à la représentation suivante.[à la ligne]En attendant, elle a le temps de se reposer, et de ramasser discrètement des objets de décoration qui, abîmés, seraient sinon jetés. Elle a amassé toute une collection de souvenirs, avec le temps.".
Le printed name est "Sur la scène".

Instead of going when la location est l' estrade, dire "Elle ne peut pas s'enfuir comme cela ! Le théâtre est toute sa vie ! Et puis, elle n'arrive pas se faire à l'idée qu'il la verrait prendre la poudre d'escampette.".

Instead of exiting when la location est l' estrade, essayer de going ouest.

Every turn when le compteur de tours est 3:
	dire "Elle n'a pas le temps de faire quoi que ce soit d'autre, le garçon vient de la saisir, afin de la ranger.";
	attendre une touche;
	maintenant le player est dans le grenier.



Chapter 1 - Joueur

La marionnette est une femme dans l' estrade. Le player est la marionnette.
Le printed name est "elle-même".
La description est "[parmi]Elle se regarde les mains, les bras, les jambes, fait jouer les articulations. Les mécanismes fonctionnent à merveille. Le bois, sous la peinture et le vernis, glisse et tourne sans aucun bruit ; contrairement à sa robe en papier vert qui bruisse à chacun de ses mouvements. Quant à son visage, elle ne peut pas le voir sans miroir. Elle ne veut pas non plus, de toute façon[ou]D'après ce qu'elle peut en juger, tout fonctionne normalement. Seul son visage… De toute façon, elle n'est pas en mesure de le voir sans miroir de quelque sorte[stoppant].".

Chapter 2 - Fils

Les fils sont une partie de la marionnette.
La description est "Quand elle est en représentation, elle doit se tenir plus ou moins tranquille. Ce sont les fils, attachés à chacune de ses articulations, qui la dirigent. Elle ne fait que donner plus d'émotion aux mouvements qu'ils dictent.[à la ligne]Quand elle n'est pas en représentation, ils deviennent des objets encombrants, menaçant de l'empêtrer à chacun de ses gestes. Mais elle s'y est faite. C'est grâce à eux qu'elle peut être sur scène. Et qu'elle l'a connu[si la location est le grenier et les fils sont une partie de la marionnette]. En ce moment, elle est suspendue à une poutre du plafond[sinon si la location est le grenier]. Elle n'y est plus attachée, maintenant[fin si]."
Comprendre "fil" comme les fils.

Instead of taking les fils, dire "Impossible : ils étaient attachés à ses poignets, ses coudes, ses genoux…[à la ligne]".

Instead of cutting les fils, dire "Sans eux, elle ne pourra pas être présente aux spectacles suivants. Et puis, elle n'a rien qui permette de les couper.".

Instead of tying les fils to something, dire "Elle ne voulait pas rendre ses mouvements plus difficiles qu'ils ne l'étaient déjà.".

Instead of tying quelque chose to les fils, essayer de tying les fils to le nom.

Chapter 3 - Croix d'attelle

La croix d'attelle est une partie des fils.
La description est "Quand elle monte sur scène, la croix s'élève vers le ciel, entraînant les fils qui y sont attachés, et le spectacle commence. Autrement, elle constitue plus un fardeau qu'autre chose. Elle s'y accommode toutefois : sans la croix d'attelle, elle ne serait rien[si la location est le grenier et les fils sont une partie de la marionnette]. En ce moment, croix d'attelle est accrochée à une poutre du plafond[sinon si la location est le grenier]. Les fils y sont suspendus, sans rien au bout[fin si].".
Comprendre "attelle" ou "d' attelle" comme la croix.

Instead of taking la croix, dire "Elle était acrochée aux fils, et essayer de prendre la croix lui faisant prendre le risque de s'empêtrer.".

Instead of tying la croix to something, dire "Elle ne voulait pas rendre ses mouvements plus difficiles qu'ils ne l'étaient déjà.".

Instead of tying something to la croix, essayer de tying les fils to le noun.

Chapter 4 - Rideaux

Les rideaux sont des choses décoratives dans l' estrade.
La description est "Ils sont fermés. Et elle n'ose pas en écarter les pans rouges pour lui jeter un dernier regard.".
Comprendre "pan/pans" ou "rideau" comme les rideaux.

Instead of opening les rideaux, dire "Non, elle n'est pas capable de le regarder une dernière fois avant qu'on vienne la suspendre.".

Instead of pulling les rideaux, essayer d' opening les rideaux.

Part 3 - Grenier

Le grenier est un endroit.
Le printed name est "Suspendue".
La description est "On l'a accroché là, en attendant la prochaine représentation. En vérité, elle passe plus de temps suspendue à ses fils, près de la fenêtre du grenier, que sur la scène. Elle trouve cela triste, mais elle préfère de loin être ici plutôt que d'être rangé dans la malle, au fond de la pièce.".

Instead of dropping something when la location est le grenier et les fils sont une partie de la marionnette et la position du pendule est 2:
		essayer de putting le nom on le rebord;
		arrêter l'action;

After dropping something when la location est le grenier:
	dire "Elle lâche [le noun] et [le] regarde tomber jusqu'au plancher.".

After putting something on le rebord:
	dire "Elle est suffisamment proche du rebord pour y poser [le nom].".

Instead of throwing something at le rebord:
	si la position du pendule est 2:
		essayer de putting le nom on le rebord;
	sinon:
		si le nom est les morceaux de papier:
			maintenant les morceaux de papier sont dans le grenier;
			dire "Elle lance les morceaux de papier vers le rebord mais, évidemment, après une brève accélération, ils flottent lentement vers le sol.";
		sinon:
			maintenant le nom est sur le rebord;
			dire "Elle lance [le noun] vers le rebord. [Il] atterrit à l'endroit même où elle avait visé.".

Chapter 1 - Fenêtre

La fenêtre est un privately-named scenery dans le grenier.
Comprendre "fenetre" comme la fenêtre.
La description est "Il fait beau dehors. La maison dans laquelle elle se trouve est en pleine campagne. La beauté et la sérénité du paysage lui donnent presque envie de sourire. Presque, car elle veut que son premier sourire lui soit destiné[première fois].[saut de paragraphe]Elle distingue à peine [--] heureusement [--] son reflet dans la vitre[seulement].".
Comprendre "fenetre" ou "vitre" comme la fenêtre.
Understand "reflet" as la marionnette when le player can see la fenêtre and we have examined la fenêtre.

Instead of examining la marionnette in the presence of la fenêtre when we have examined la fenêtre and le rat n'est pas dans le grenier:
	dire "Elle est suspendue suffisamment près de la vitre pour que cette dernière reflète son visage. En règle générale, elle évite de se regarder, mais un unique coup d[']œil accidentel lui rappelle la triste vérité.";
	attendre une touche;
	dire "Peints sur son visage de bois verni, ses beaux yeux verts ne cillent jamais et, le plus terrible, ses lèvres restent figées dans une éternelle expression de neutralité.";
	attendre une touche;
	dire "Machinalement, elle porte une main à sa bouche et suit le contour de ses lèvres avec son index. Arrivera-t-elle à sourire un jour ?[saut de paragraphe]";
	attendre une touche;
	dire "« J'peux t'aider si tu veux, » lui dit soudain une voix. Elle n'a pas entendu arriver le rongeur qui vient de grimper sur le rebord de la fenêtre. Peut-elle lui faire confiance ?";
	maintenant le rat est dans le grenier;
	établir les pronoms pour le rat.

Instead of examining la marionnette in the presence of la fenêtre when we have examined la fenêtre:
	dire "Elle ne compte pas se regarder de nouveau. Pas avant que son problème soit résolu.".

Chapter 2 - Rebord

Le rebord est un out of reach support.
Le rebord est dans le grenier.
La description est "Le rebord de la fenêtre.".

Before listing nondescript items of le grenier:
	let L be la list of things which are not le rat on the rebord;
	si le nombre d'entrées de L est 0, maintenant le rebord n'est pas marqué pour le listage.

Rule for writing a paragraph about le rebord:
	let L be la liste de choses which are not le rat sur le rebord;
	si le nombre d'entrées de L est supérieur à 0:
		si la paire de ciseaux est sur le rebord et les morceaux de papier sont sur le rebord et le nombre d'entrées de L est 2:
			dire "Des ciseaux sont posés sur le rebord de la fenêtre, à côté de restes de papier colorés.";
			maintenant la paire de ciseaux est mentionnée;
			maintenant les morceaux de papier sont mentionnés;
		sinon:
			dire "[Une liste de choses which are not le rat sur le rebord] [es] [posé] sur le rebord de la fenêtre.".

Instead of entering le rebord when la position du pendule est 2, dire "Elle ne peut pas : les fils la retiennent.".

Chapter 3 - Ciseaux

La paire de ciseaux est une chose out of reach sur le rebord.
La description est "Cette paire de ciseaux doit appartenir à l'enfant qui habite dans la maison[si non manipulé]. Elle a dû servir à découper les papiers à côté desquels elle a été posée, sur le rebord de la fenêtre[fin si].".

After taking la paire de ciseaux when la paire de ciseaux n'était pas manipulée:
	maintenant la paire de ciseaux est manipulée;
	dire "Elle parvient à prendre les ciseaux, puisqu'elle est suffisamment proche du rebord.".

After dropping la paire de ciseaux when les fils sont une partie de la marionnette et la location est le grenier et la position du pendule n'est pas 2:
	dire "Elle pense qu'elle en a besoin, et elle ne va pas les lâcher.".
	[maintenant le rat est off-stage;
	dire "Elle lâche les ciseaux, qui tombent sur le plancher, plus bas.";
	attendre une touche;
	dire "« Hmph, » laisse échapper le rat. Il n'a pas l'air content.";
	attendre une touche;
	dire "« Tu pourras plus descendre maintenant. Laisse tomber, je trouverais quelqu'un d'autre à bern… pour m'aider. » Puis il s'en va.";
	attendre une touche;
	dire "Que fera-t-elle, maintenant ?";]

Instead of cutting les fils when le player porte la paire de ciseaux et les fils sont une partie de la marionnette:
	maintenant les fils sont dans le grenier;
	maintenant les fils sont out of reach;
	maintenant la croix est out of reach;
	maintenant la malle est nearby;
	maintenant le printed name du grenier est "Sur le plancher du grenier";
	maintenant la description du grenier est "Elle se trouve maintenant sur le plancher du grenier. [Les fils] qui la retenaient et qui sont maintenant suspendus à une poutre du plafond lui font douter un instant : a-t-elle fait ce qu'il fallait faire ?";
	dire "Décidée, elle coupe les fils qui la retiennent avec les ciseaux. Cela retardera la prochaine fois qu'elle montera sur scène, mais quand elle sera réparée, elle sourira. Elle en est convaincue.".

Chapter 4 - Morceaux de papier

Les morceaux de papier sont une chose out of reach sur le rebord.
Comprendre "morceau", "papiers", "reste" ou "restes" comme les morceaux.
La description est "Des morceaux de papier multicolores, probablement découpés par l'enfant qui habite ici. Il a sûrement fabriqué un nouveau décor pour le prochain spectacle.".

After taking les morceaux de papier when les morceaux de papier n'étaient pas manipulés:
	maintenant les morceaux de papier sont manipulés;
	dire "Elle parvient à prendre les morceaux de papier, puisqu'elle est suffisamment proche du rebord.".

Chapter 5 - Rat

Le rat est un animal out of reach. "Le rat se tient debout sur le rebord, les moustaches frémissantes.".
Comprendre "rongeur" comme le rat.
La description est "La nuit, elle a déjà aperçu des rats courant sur le plancher, mais elle se sentait en sécurité suspendue à cette hauteur.[à la ligne]Celui-là semble aveugle, et son museau ne s'arrête jamais de frémir. Elle ne le connaît pas, et il ne lui inspire pas confiance. Devrait-elle l'écouter ?".

Does the player mean talking to le rat when la location est la malle-endroit:
	it is likely.

After talking to le rat pour la première fois:
	dire "« Alors, tu veux que j't'aide ? »[à la ligne]";
	attendre une touche;
	dire "Elle réfléchit un instant… « Oui », pense-t-elle.";
	attendre une touche;
	dire "Elle hoche la tête.[saut de paragraphe]";
	attendre une touche;
	dire "« T'as raison ! Mais d'abord, tu dois m'aider, hein. C'est donnant-donnant, tu comprends ? J'espère bien en tout cas, car on n'voudrait pas rester comme cela, si ? »[saut de paragraphe]";
	attendre une touche;
	dire "Quelque chose dans le ton du rongeur ne lui dit rien qui vaille. Mais son désir de sourire est plus fort que sa méfiance envers ce rat. Elle acquiesce lentement.[saut de paragraphe]";
	attendre une touche;
	dire "« Tu vois la malle ? Elle est truffée de pièges à rats et de poison, mais elle contient quelque chose qui m'revient de plein droit : une précieuse gemme que m'a léguée mon père, et qui m'a été volée par le propriétaire d'cette maison. Débrouille-toi comme tu l'peux, j'veux rien savoir, apporte-moi juste cet objet et j't'aiderai. »[à la ligne]".

After talking to le rat:
	dire "« Hé, j't'attendrai pas là éternellement ! »[à la ligne]".

Chapter 6 - Se balancer dans le grenier

Instead of swinging la marionnette when we have not talked to le rat:
	dire "Pourquoi faire cela ? Elle doit simplement attendre la prochaine représentation." instead.

Instead of swinging la marionnette when we have talked to le rat, dire "Elle se donne une impulsion et commence à s'éloigner de la fenêtre.".

La position du pendule est initialement 0.
[-2 = au plus loin de la fenêtre
-1 = s'éloigne de la fenêtre
0 = immobile
1 = s'approche de la fenêtre
2 = est à côté de la fenêtre]

Pendule est une scène.
Pendule begins when we are swinging la marionnette and we have talked to le rat.
Pendule ends when les fils ne sont pas une partie de la marionnette.

When pendule begins, maintenant la position du pendule est -1.

[TODO: varier les messages atmosphériques]
Every turn during Pendule:
	let x be la position du pendule;
	si x est -1:
		maintenant la position du pendule est -2;
		dire "Elle est maintenant au plus loin de la fenêtre.";
	sinon si x est -2:
		maintenant la position du pendule est 1;
		dire "Elle revient vers la fenêtre.";
	sinon si x est 1:
		maintenant la position du pendule est 2;
		maintenant le rebord est nearby;
		si les morceaux de papier ne sont pas manipulés, maintenant les morceaux de papier sont nearby;
		si la paire de ciseaux n'est pas manipulée, maintenant la paire de ciseaux est nearby;
		dire "Elle est si proche de la vitre qu'elle peut la toucher.";
	sinon si x est 2:
		maintenant la position du pendule est -1;
		maintenant le rebord est out of reach;
		si les morceaux de papier ne sont pas manipulés, maintenant les morceaux de papier sont out of reach;
		si la paire de ciseaux n'est pas manipulée, maintenant la paire de ciseaux est out of reach;
		dire "Elle s'éloigne de la vitre.".

Part 4 - Intérieur de la malle

Before going to la malle-endroit:
	dire "Elle soulève avec peine le couvercle de la malle et se faufile dans la fente créée avant de la laisser retomber.".

Before going from la malle-endroit:
	dire "Elle pousse le couvercle difficilement et sort à l'air libre.".

La malle-endroit est un endroit sombre privately-named.
Le printed name est "À l'intérieur de la malle"

Rule for printing the name of a dark room when la location est la malle-endroit:
	dire "À l'intérieur de la malle".

Rule for printing the description of a dark room when la location est la malle-endroit:
	dire "[parmi]Un frisson la parcourerait si elle n'était pas faite de bois : l[ou]L[stoppant]a malle est obscure et elle n'y voit goutte. À vrai dire, elle ne sait même pas qui ou quoi est entreposé ici. Une étrange sensation s'empare d'elle alors qu'il lui semble entendre des voix.".

Every turn when la location est la malle-endroit et we are not listening et we are not talking to:
	dire "[parmi]Un faible chuchotement se fait entendre[ou]Elle sent un faible souffle dans sa nuque[ou]Un mouvement attire son regard vers la droite, mais elle ne peut évidemment rien distinguer[ou]Un mouvement attire son regard vers la gauche, mais elle ne peut évidemment rien distinguer[ou]Un grincement lugubre s'élève un instant quelque part devant elle, puis disparaît[au hasard].".

Instead of listening to la malle-endroit:
	dire "[parmi]Elle distingue des bribes de paroles, à peine audibles malgré le silence[ou]Une voix semble s'élever, quelque part à côté d'elle[ou]Un chuchotement se fait entendre[ou]Des paroles sont murmurées dans l'obscurité[ou]Des mots sont prononcés tout bas[au hasard] : « … [parmi]quelqu'un est vraiment entré ?[sans passage à la ligne][ou]faites pas de mal ![sans passage à la ligne][ou]nous veut-elle[ou]chht, pas si fort[ou]ai peur ![sans passage à la ligne][ou]taisez-vous voyons ! Elle va[ou]va-t-on nous emmener ?[sans passage à la ligne][au hasard] … »[à la ligne]".

Chapter 1 - Malle (porte)

La malle est une décorative non ouvrable ouverte porte.
La malle est à l'intérieur du grenier. La malle est à l'extérieur de la malle-endroit.
Le printed name est "[si la location est la malle-endroit]couvercle de la [fin si]malle".

Carry out going to la malle-endroit from le grenier:
	maintenant la malle est male;
	maintenant la malle est masculine gender.

Carry out going to le grenier from la malle-endroit:
	maintenant la malle est female;
	maintenant la malle est feminine gender.
	
Chapter 2 - Voix

Les voix (f) sont un scenery dans la malle-endroit.
Comprendre "paroles/paroles/chuchotement/chuchotements/mots" comme les voix.

Instead of examining les voix:
	essayer de listening to les voix.

Instead of listening to les voix:
	essayer de listening to la malle-endroit.

Instead of doing something other than examining, talking to or listening with les voix:
	dire "Des voix, tels des fantômes, hantent cet endroit. Elle devrait plutôt les écouter ; ou bien aura-t-elle le courage de leur parler ?".

After deciding the scope of the player while la location est la malle-endroit:
	placer les voix à la vue.

Does the player mean talking to les voix when la location est la malle-endroit:
	it is very likely.

Instead of talking to les voix pour la première fois:
	dire "Elle essaie de parler pour se donner du courage :[saut de paragraphe]";
	attendre une touche;
	dire "« Y a-t-il quelqu'un ? »[saut de paragraphe]";
	attendre une touche;
	dire "Alors que l'endroit paraissait rempli de spectres murmurants quelques minutes auparavant, il est maintenant plongé dans le silence le plus total. Bravant sa peur, elle décide de continuer :[saut de paragraphe]";
	attendre une touche;
	dire "« Je cherche une pierre précieuse ; elle appartient à un rat. »[saut de paragraphe]";
	attendre une touche;
	dire "Le silence semble s'épaissir. Elle s'apprête à poursuivre quand quelqu'un, dans le noir, plus proche qu'elle ne l'aurait pensé, répond:[saut de paragraphe]";
	attendre une touche;
	dire "« Tu arrives trop tard. Cela fait longtemps que personne ne nous utilise ; cette malle est destinée aux objets que l'enfant qui habite ici a oubliés.";
	attendre une touche;
	dire "Pas plus tard qu'hier, son père est venu. Il a ouvert la malle, a choisi qui resterait et qui partirait. Celui qui détient ce que tu cherches était parmi ceux qui sont partis.[saut de paragraphe]";
	attendre une touche;
	dire "[--] Vous voulez dire qu'il a été…[saut de paragraphe]";
	attendre une touche;
	dire "[--] Oui. Jeté avec les autres. La poubelle doit être dehors, près du portail, dans le jardin. »[saut de paragraphe]";
	attendre une touche;
	dire "Le silence retombe, encore plus pesant. Elle se demande si les efforts qu'elle devra fournir en vaudront la peine. Mais il est un peu tard pour regretter maintenant. Elle est prête à repartir.[saut de paragraphe]";
	attendre une touche;
	dire "« Merci, dit-elle, reconnaissante. Je ferai tout ce que je peux pour vous sortir d'ici. » Même si au fond, elle sait qu'elle ne pourra rien faire.";
	la règle réussit.

Instead of talking to les voix, dire "Elle savait tout ce qu'il y avait à savoir. Elle devait ressortir.".

Instead of going from la malle-endroit to le grenier when we have talked to les voix:
	dire "Comme elle sort de la malle, elle entend des bruits de pas qui se rapprochent. Elle s'immobilise sur le sol.[saut de paragraphe]";
	attendre une touche;
	dire "C'est l'enfant qui revient. Il est très surpris en voyant la marionnette à terre. Il la répare rapidement avec du ruban adhésif, puis la prends et redescend.[saut de paragraphe]";
	attendre une touche;
	maintenant les fils sont nearby;
	maintenant la croix est nearby;
	maintenant les fils sont une partie de la marionnette;
	retirer les ciseaux du jeu;
	retirer les morceaux de papier du jeu;
	maintenant la marionnette est dans l' estrade2;
	jouer la pièce.

Part 5 - Estrade 2

L' estrade2 (f) est un endroit privately-named. "Elle est de retour sur la scène ; il n'est pas là aujourd'hui. « Tant mieux, » pense-t-elle, elle pourra mieux se concentrer sur sa tâche. Et la prochaine fois, il la verra sourire.[à la ligne]Son plan est le suivant : à la fin de sa représentation, elle s'enfuira par la trappe située au milieu de l'estrade, puis elle essaiera d'atteindre le jardin. Pour l'heure, elle doit jouer la pièce que les fils lui dictent.".
Le printed name est "Sur la scène".

Chapter 1 - Les rideaux

Les rideaux2 sont des privately-named scenery dans l' estrade2.
La description est "Ils sont fermés. Plus personne ne la voit, il est temps pour elle de s'en aller !".
Comprendre "pan/pans" ou "rideau" comme les rideaux2.

Instead of opening les rideaux2, dire "Non, elle n'a pas le temps, elle doit s'en aller !".

Instead of pulling les rideaux2, essayer de opening les rideaux2.

Chapter 2 - Maison

La maison est une chose dans l' estrade2.
La maison can be attachée. La maison is not attachée.
La description est "Une jolie petite maison, décor de la pièce qu'elle vient de jouer. Elle est en bois[si la trappe n'est pas décrite]. Il semblerait que la trappe soit en dessous[fin si].".

Instead of entering la maison, dire "Ce n'est pas une vraie maison de poupée, ce n'est qu'un décor. Elle est bien trop petite pour que l'on y entre.".

Instead of opening la maison, essayer d' entering la maison.

Instead of pushing la maison:
	dire "[si la trappe n'est pas décrite]Elle est trop lourde. Peut-être en la tirant ?[sinon]La trappe est juste là, elle n'a plus de temps à perdre ![fin si]".

Instead of pulling la maison:
	si la trappe est décrite:
		essayer de pulling la maison;
	sinon si la maison est attachée:
		dire "Elle tire de toutes ses forces sur les fils. Rien ne se passe au début, puis la maison commence à bouger. Une trappe apparaît !";
		maintenant la trappe est décrite;
	sinon:
		dire "Elle est trop lourde. Peut-être que si elle trouvait un autre moyen de la tirer ?".

Instead of pulling les fils when la maison est attachée, essayer de pulling la maison.

Instead of tying la croix to la maison:
	si la maison est attachée:
		dire "Elle est déjà attachée à la maison !";
	sinon:
		dire "Elle accroche la croix d'attelle à la maison.";
		maintenant la maison est attachée.

Instead of tying la maison to la croix, essayer de tying la croix to la maison.
Instead of tying les fils to la maison, essayer de tying la croix to la maison.
Instead of tying la maison to les fils, essayer de tying la croix to la maison.

Chapter 3 - Barrière

La barrière est une chose fixée sur place privately-named dans l' estrade2.
Comprendre "barriere" comme la barrière.
La description est "Ce décor entoure la maison. L'une de ses planches est mal clouée.".

La planche est une partie de la barrière.
La description est "[si la planche est une partie de la barrière]Cette planche ne tient plus que par un clou. Et encore, le clou lui-même ne semble pas vraiment tenir non plus[sinon]Une planche qu'elle a arrachée de la barrière. Elle paraît robuste[fin si].".

Instead of taking la planche:
	maintenant le player porte la planche;
	dire "Elle tire un peu, et la planche cède. À quoi pourrait-elle servir ?".

Instead of pulling la planche, essayer de taking la planche.
Instead of attacking la barrière, essayer de taking la planche.

Chapter 4 - Trappe

La trappe est en dessous de l' estrade2. Through the trappe is la cache.
La trappe est une non décrite non ouvrable porte.
La description est "Il s'agit de la trappe permettant de faire apparaître des décors comme par magie [--] ou de faire disparaître une marionnette, dans le cas présent. Elle n'a jamais servi, elle espère qu'elle s'ouvrira sans problème.".

Instead of doing something with la trappe when la trappe n'est pas décrite:
	dire "Elle ne voit rien de tel, à moins que cela ne soit sans grande importance.".

Instead of going bas from l' estrade2 when Pendule has ended and la trappe is undescribed:
	dire "Elle ne peut pas aller par là.".

Instead of opening la décrite fermée trappe:
	si la marionnette porte la planche:
		dire "Elle pourrait peut-être essayer de la forcer avec quelque chose ?";
	sinon:
		dire "La trappe est bloquée, elle ne parviendra pas à l'ouvrir avec ses mains.".

Instead of unlocking la trappe with la planche:
	maintenant la trappe est ouverte;
	dire "Elle parvient à caler la planche dans l'interstice, et appuie de toutes ses forces. La trappe finit par s'ouvrir !".

Instead of going bas when la location est l' estrade2 et la trappe est ouverte:
	retirer les fils du jeu;
	dire "Rapidement, elle se jette dans la trappe pour tomber sous la scène. C'est là qu'elle entrepose tous ses trésors. Mais elle n'a pas le temps de les contempler.";
	attendre une touche;
	dire "Elle se précipite vers la sortie, mais la croix d'attelle est restée coincée dans la trappe. Elle tire d'un coup sec et le rafistolage que l'enfant a fait juste avant la représentation cède. Tant pis pour les dégâts.[saut de paragraphe]";
	attendre une touche;
	dire "Pendant que les habitants de la maison sont dans le salon à applaudir, elle se faufile de pièce en pièce et sort dans le jardin.";
	attendre une touche;
	maintenant la marionnette est dans le jardin.

Instead of entering la trappe, essayer de going bas.

Chapter 5 - Pièce de théâtre

Table of Drama
réplique	description
"se balader"	"Elle fait semblant de sortir d'une maison et se balade autour. Elle est seule dans cette histoire."
"cueillir des fleurs"	"Elle cueille des fleurs imaginaires, et les met dans un vase. Celui avec qui elle habite n'est pas rentré depuis longtemps, et elle fait ce rituel chaque jour, au cas où il reviendrait."
"s'asseoir"	"Elle s'assoit et est censée penser à l'homme avec qui elle vivait. Elle sait qu'il va revenir, et c'est pour cela qu'elle prend soin de la maison. C'est une façon de dire qu'elle songé à lui pendant tout ce temps[saut de paragraphe][romain]Et elle continue la pièce de cette façon, jusqu'à ce que la représentation se termine. Enfin ! Elle sourira à la fin de la journée, elle en certaine."

To jouer la pièce:
	parcourir la Table of Drama:
		dire "[command prompt][input style]";
		let réplique be the substituted form of "[réplique entry]";
		répéter avec T allant de 1 à nombre de caractères dans réplique:
			attendre une touche;
			dire "[caractère numéro T dans réplique]";
		attendre une touche;
		dire "[italique][à la ligne][description entry][saut de paragraphe]".

Part 6 - Jardin

Le jardin est un endroit. "Le jardin de la maison est bien entretenu et les plantes se gorgent du soleil rayonnant. Elle se trouve sur une allée de gravier et devant elle se dresse la poubelle. Elle espère que celui qu'elle cherche est encore là. C'est sa dernière chance.".
Le printed name est "Dans le jardin".

Instead of going when la location est le jardin, dire "Elle doit se concentrer sur sa tâche.".
Instead of going inside when la location est le jardin, dire "Il n'est pas encore temps de rentrer !".

Chapter 1 - Poubelle

La poubelle est une chose dans le jardin.
La description est "La poubelle se situe au bout de l'allée, juste avant le portail. Elle doit se dépêcher.".

Instead of entering la poubelle:
	dire "Elle escalade péniblement la poubelle, pousse son couvercle, qui tombe à terre, et saute à l'intérieur.";
	maintenant la marionnette est dans la poubelle-endroit.

Chapter 2 - Plantes

Les plantes sont des choses décoratives dans le jardin.
La description est "Ces plantes sont magnifiques. Les propriétaires en prennent vraiment soin.".
Comprendre "plante/arbre/arbres/fleur/fleurs" comme les plantes.

Chapter 3 - Portail

Le portail est une chose décorative dans le jardin.
La description est "Un portail peint en blanc. Il n'a rien de spécial.".

Instead of opening le portail, dire "Elle devrait se concentrer sur la poubelle. De toute façon, si elle avait véritablement besoin de passer, il lui suffirait de se glisser entre les barreaux.".

Part 7 - Poubelle (endroit)

La poubelle-endroit est à l'intérieur du jardin. "La poubelle est à moitié pleine. Il y a là de nombreux détritus sans importance, et qu'elle aurait préféré de ne jamais les avoir vu de sa vie. Elle se retient de fuir, car elle touche au but.".
La poubelle-endroit est privately-named.
Le printed name est "À  l'intérieur de la poubelle".

Instead of smelling la poubelle-endroit, essayer de smelling les détritus.

Instead of going dehors when la location est la poubelle-endroit:
	dire "La poubelle n'est pas remplie, et le rebord est trop haut. En outre, les parois sont devenues glissantes à cause des ordures.".

Chapter 1 - Détritus

Les détritus sont une chose décorative privately-named dans la poubelle-endroit.
Comprendre "detritus/ordure/ordures" comme les détritus.
La description est "Absolument répugnant. Et dire que des jouets y ont été jeté. Elle en a froid dans le dos.".

Instead of smelling les détritus, essayer de examining les détritus.

Instead of searching les détritus:
	maintenant la marionnette porte le cintre;
	dire "Réprimant son dégoût, elle se met à fouiller les ordures sous ses pieds. Elle trouve d'autres jouets jetés eux aussi et bloqués sous des immondices. Enfin, elle met la main sur quelque chose d'intéressant : un cintre."

Chapter 2 - Jouet

La figurine est un homme dans la poubelle-endroit. "Une figurine est assise sur un monticule d'ordures, résignée au sort qui l'attend.".
La description est "Un jouet pour garçon, sûrement un justicier ou un superhéros. Il tient quelque chose.".

Instead of talking to the figurine:
	dire "Elle s'apprête à parler à la figurine, mais cette dernière est plus rapide qu'elle :[saut de paragraphe]";
	attendre une touche;
	dire "« Qui est-tu ? Que viens-tu faire ici ? Tu n'a pas été jetée comme nous, sinon je te connaîtrais.[saut de paragraphe]";
	attendre une touche;
	dire "[--] Je… C'est l'un des vôtres qui m'a indiqué que je pourrais vous trouver ici. Je cherche une gemme, que l'on m'a demandé de trouver. L'auriez-vous vue ?[saut de paragraphe]";
	attendre une touche;
	dire "[--] Une pierre précieuse ? Non, la seule chose que je possède, c'est ce rubis en plastique. Il a toujours été dans la malle, et je ne sais pas pourquoi, j'ai décidé de le garder avec moi. »[saut de paragraphe]";
	attendre une touche;
	dire "Elle se fige. Elle a risqué tout ce qui lui était cher pour un bout de plastique, qui de surcroît n'appartient pas au rat ?[saut de paragraphe]";
	attendre une touche;
	dire "« Vous voulez dire que… Tant pis, laissez tomber. Je… »[saut de paragraphe]";
	attendre une touche;
	dire "Elle n'a pas le temps de finir. Un vrombissement secoue la poubelle en entier.[saut de paragraphe]";
	attendre une touche;
	dire "« Qu'est-ce que c'est ?[saut de paragraphe]";
	attendre une touche;
	dire "[--] C'est le camion poubelle ! s'exclame la figurine. C'est la fin. »[saut de paragraphe]";
	attendre une touche;
	dire "La marionnette réfléchit. Non, elle n'aura pas fait tout ce chemin pour rien. « Pas si je vous aide à sortir, » affirme-t-elle, déterminée.".

Chapter 3 - Rubis en plastique

Le rubis en plastique est une chose.
La description est "[parmi]Cette pierre précieuse n'en est pas une ! C'est juste une babiole en plastique ! Et dire qu'elle a fait tout ce chemin pour cette chose ! Le rat s'est bien moqué d'elle ! Au diable ce maudit rongeur ! Que fera-t-elle maintenant qu'elle sait qu'il ne la fera pas sourire ?[ou]Cette « gemme » est en plastique. Vraiment. Elle a risqué tout ce qui lui était cher [--] la scène, et [italique]lui[romain] aussi [--] pour cette breloque. Et en plus, elle ne sourira jamais. Il est évidemment que le rat s'est moqué d'elle. Tout est perdu, semble-t-il.[stoppant]".
La figurine porte le rubis.
Comprendre "quelque chose" comme le rubis.

Chapter 4 - Cintre

Le cintre est une chose.
La description est "Ce cintre a l'air souple mais robuste. Elle devrait peut-être en faire quelque chose.".
Le cintre peut être défait. Le cintre est non défait.

Instead of squeezing le cintre:
	maintenant le cintre est défait;
	dire "Elle tord le cintre et le défait. À présent, il ressemble à une longue perche terminée par un crochet.".

Chapter 5 - Rebord-poubelle

Le rebord-poubelle est une chose décorative privately-named dans la poubelle-endroit.
Le printed name est "rebord".
Comprendre "rebord" comme le rebord-poubelle.
La description est "Le rebord est bien trop haut, elle ne peut pas sortir.".

Instead of tying le cintre to le rebord-poubelle:
	si le cintre est défait:
		dire "Elle accroche prestement le bout du cintre au rebord de la poubelle. « Vite, grimpez ! » ordonne-t-elle.[saut de paragraphe]";
		attendre une touche;
		dire "Les jouet réagissent aussitôt. Un par un, il escalade la « corde » de fortune et s'échappe. Elle jette un dernier coup d[']œil autour d'elle pour s'assurer que tout le monde est sorti et attrape le cintre.[saut de paragraphe]";
		attendre une touche;
		dire "Au même moment, une secousse se produit et elle lâche prise. Elle retombe sur le tas d'immondices.[saut de paragraphe]";
		attendre une touche;
		dire "Elle ne comprendra sans doute jamais la suite des événements.[saut de paragraphe]";
		attendre une touche;
		dire "Elle se retrouve à l'intérieur du camion poubelle.";
		attendre une touche;
		dire "Elle nage dans un océan de détritus pour atteindre la sortie avant qu'il ne soit trop tard.";
		attendre une touche;
		dire "Elle tombe sur la chaussée, pas trop loin du portail de la demeure dans laquelle elle a passé toute sa vie.";
		attendre une touche;
		dire "Elle s'évanouit là, avant de se réveiller.[saut de paragraphe]";
		attendre une touche;
		dire "Sur la scène. Les fils dictant ses mouvements. Le garçon l'avait retrouvé dans la rue, avant de la réparer.[saut de paragraphe]";
		attendre une touche;
		dire "[italique]Il[romain] est là, lui aussi. Et elle sait qu'elle n'a pas besoin de lui sourire, maintenant. Des gestes peuvent faire toute la différence, comme quand elle a sauvé tous ces jouets.[saut de paragraphe]";
		attendre une touche;
		dire "La représentation est terminée. Elle le fixe dans les yeux, sans essayer de se cacher, et lui fait un geste de la main.[saut de paragraphe]";
		attendre une touche;
		dire "Et il lui sourit en réponse.";
		attendre une touche;
		fin définitive de l'histoire;
	sinon:
		dire "Elle ne peut rien faire avec le cintre en l'état.".

Volume 3 - Parution

Book 1 - Données bibliographiques

Le story headline est "Un conte interactif".
Le story genre est "Conte".
Le release number est 1.
Le story creation year est 2014.
Le story description est "Les rideaux se referment. Juste avant qu'ils ne se rejoignent, elle le regarde fugacement dans les yeux et tente de lui sourire. En vain. Ses lèvres ne bougent pas. Elle n'y arrive tout simplement pas. Malgré tous ses efforts, son visage reste impassible. « À la prochaine représentation, je le ferai, » se promet-elle comme à chaque fois".

Volume 4 - Tests

Test me with "examiner les rideaux / examiner les fils / examiner la croix / regarder la vitre / me regarder / lui parler / me balancer / attendre / attendre / prendre la paire de ciseaux / attendre / attendre / attendre / prendre les morceaux de papier / couper les fils / entrer / parler / sortir / attacher la croix à la maison / tirer la maison / arracher la planche / ouvrir la trappe avec la planche / entrer dans la trappe / entrer dans la poubelle / parler à la figurine / fouiller les détritus / tordre le cintre / accrocher le cintre au rebord".